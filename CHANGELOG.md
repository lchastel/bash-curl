# [1.1.0](https://gitlab.com/lchastel/bash-curl/compare/1.0.1...1.1.0) (2024-05-22)


### Features

* add extra tag latest ([5a97ac7](https://gitlab.com/lchastel/bash-curl/commit/5a97ac7c54b57f6e2597c908d1fad6f7fac9f9ce))

## [1.0.1](https://gitlab.com/lchastel/bash-curl/compare/1.0.0...1.0.1) (2024-05-22)


### Bug Fixes

* automatic publish image ([94e1763](https://gitlab.com/lchastel/bash-curl/commit/94e17636be30ba33ca444d3e6ffca68b7677a21d))

# 1.0.0 (2024-05-22)


### Bug Fixes

* consolidate run ([0da2f10](https://gitlab.com/lchastel/bash-curl/commit/0da2f1047d53db724cf1abaeef53523ec7700af3))
* hadolint issues ([c6c32d0](https://gitlab.com/lchastel/bash-curl/commit/c6c32d05d332fdbccc9d2047f04e03294b7a7845))
* short-name resolution enforced but cannot prompt without a TTY - FROM bash... ([d84b5b5](https://gitlab.com/lchastel/bash-curl/commit/d84b5b55c56aa03a8ed4f617dc7e6dea99a6bde7))
* upgrade packages ([1a93953](https://gitlab.com/lchastel/bash-curl/commit/1a93953a2380e158645182df36b08d2cb985b137))


### Features

* initial commit ([44d9828](https://gitlab.com/lchastel/bash-curl/commit/44d98286d15d1bc3968f9bb6ec0c02ebd6351b3c))
