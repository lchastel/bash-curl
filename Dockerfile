FROM docker.io/bash:5.2.26

RUN apk upgrade --no-cache && \
    apk add --no-cache curl=8.5.0-r0

CMD ["curl"]